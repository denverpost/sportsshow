<div id="wait" style="position: absolute; top: 200; left: 200; visibility:hidden; font-size: 1pt; color: white;">
    <!-- SiteCatalyst code version: H.17
    Copyright 1997-2005 Omniture, Inc. More info available at http://www.omniture.com -->
    <script>
    /* Specify the Report Suite ID(s) to track here */
    var s_account="denverpost";
    </script>
    <script src='http://extras.mnginteractive.com/live/js/omniture/SiteCatalystCode_H_22_1_NC.js'></script>
    <script src='http://extras.mnginteractive.com/live/js/omniture/OmniUserObjAndHelper.js'></script>
    <script>
    //Local Variables
    var PaperBrand = getBrand2(s_account);
    var PageName = "SPORTSSHOW <?php wp_title(':'); ?>";
    var SectionName = "SPORTSSHOW";
    var ArticleTitle = "";
    var oChnm = "Blogs";
    var oRtnm = "Blogs / SPORTSSHOW";

    var FriendlyName = "SPORTSSHOW <?php wp_title(':'); ?>";
    var domainName = getDomainName();
    userObj = new omniObj();
    userObj.load();
    userObj.update();
    userObj.save();
    /* You may give each page an identifying name, server, and channel on the next lines. */
    s.pageName=FriendlyName;
    s.channel="SPORTSSHOW"; // Same as prop1
    s.server="BLOGS";// Blank
    s.pageType=""; // Error pages ONLY

    s.prop1="D=g";
    beanprop2 = ("" != "") && ("" != null) ? "" : "?";
    var escbeanprop2 = escape(beanprop2);
    var unescbeanprop2 = unescape(escbeanprop2);
    var articleId = "";

    beanprop3 = ("" != "") && ("" != null) ? "" : "?";
    beanprop4 = ("" != "") && ("" != null) ? "" : "?";

    if(articleId != "" && articleId != null){
    beanprop5 = ("" != "") && ("" != null) ? "" : articleId +"_" +ArticleTitle;
    } else {
    beanprop5 = "?" ;  
    }

            //s.prop2=""; // Sub section 1
            s.prop2='D=ch+' + "\"/" + unescbeanprop2 + "\"";
            //s.prop3=""; // Sub section 2
            s.prop3='D=ch+' + "\"/" + unescbeanprop2 + "/" + beanprop3 + "\"";
            //s.prop4=""; // Sub section 3
            s.prop4='D=ch+' + "\"/" + unescbeanprop2 + "/"+ beanprop3 + "/"+ beanprop4 + "\""; // Sub section 3
            s.prop5='D=ch+' + "\"/" + unescbeanprop2 + "/" + beanprop3 + "/" + beanprop4 + "/" + beanprop5 + "\""; // Sub section 4
                
    s.prop6=""; // Global - Section
    s.prop7=""; // Global - Sub section 1
    s.prop8=""; // Global - Sub section 2
    var sourceVal = ""; // Source of request, i.e. RSS, flash, etc...
    s.prop10=""; // Reserved for RSS
    s.prop12=""; //Byline
    s.prop13=""; // Reserved for article
    s.prop14=""; // Reserved for article
    s.prop15=""; // Reserved for article
    s.prop16="";    // Search
    s.prop17="";    // Search
    s.prop18="";    // Search
    s.prop19="";    // Search
    s.prop20=""; // Reserved for Search
    s.prop21=""; // Reserved for Search
    s.prop22=""; // Reserved for Search
    s.prop23=""; // Reserved for Search
    s.prop24=""; // Reserved for Search
    s.prop25=""; // Reserved for Search
    s.prop26=""; // 3rd Party Vendors
    s.prop27=""; // OneSpot
    s.prop28=""; // Blank
    if(s.prop29 == null || s.prop29 == ''){
    s.prop29="";
    }
    s.prop30=""; // Form Analysis Plugin
    s.prop31=""; // Blank
    s.prop32=""; // Blank
    s.prop33=ArticleTitle=="null"?'D=c40+" / "+c43':'D=c40+" / "+c50+" / "+c9';
    s.prop34=ArticleTitle=="null"?'D=c40+" / "+c43':'D=c40+" / "+c43+" / "+c50';
    s.prop35=ArticleTitle=="null"?'D=v18=" / "+c40+" / "+c43':'D=v18+" / "+c40+" / "+c50';
    s.prop36=isCampaign(getCampaignValue("EADID")+getCampaignValue("CREF"), PaperBrand, PageName, ArticleTitle); // Campaign Tracking Code  + Paper Brand + Page Name
    s.prop37=isCampaign(getCampaignValue("IADID")+getCampaignValue("SOURCE"), PaperBrand, PageName, ArticleTitle); // Affiliate ID + Paper Brand + Page Name
    s.prop38=isCampaign(getCampaignValue("PARTNERID"), PaperBrand, PageName,ArticleTitle); // Internal Referral ID + Paper Brand + Page Name
    s.prop39="";                                                             // Search Engine + Keywords + Paper Brand + Page Name (populated by functions.js)
    s.prop40=PaperBrand;                                                     // Paper Brand
    s.prop41="";                                                             // Blank
    s.prop42="";
    s.prop43=SectionName; //
    s.prop44=ArticleTitle=="null"?"":'D=c40+" / "+c43+" / "+v26';
    s.prop45=ArticleTitle=="null"?"":'D=c40+" / "+c43+" / "+c12';
    s.prop46=getArticleHelperPage(domainName,"",location.href,ArticleTitle); // ArticleID + Special Page Name + Article Title
    s.prop47=""; // Search
    s.prop48=""; // Blank
    s.prop49=""; // Blank
    s.prop50=ArticleTitle;
    /* E-commerce Variables */
    s.campaign=getCiQueryString("EADID")+getCiQueryString("CREF");          //External Campaign - ?EADID=id
    s.state="";
    s.zip="";
    s.events=getEvents(ArticleTitle, s.events);
    s.products="";
    s.purchaseID="";
    s.eVar1=getCiQueryString("PARTNERID");                                  // Internal Campaign - ?PID=id
    s.eVar2=getCiQueryString("IADID")+getCiQueryString("SOURCE");           // Affiliate ID - ?IADID=id
    s.eVar3=getBrandOnChange(PaperBrand);                                             //Paper Brand
    s.eVar4="D=pageName";
    s.eVar5="";
    s.eVar6="";
    s.eVar7="";
    s.eVar8="";
    s.eVar9="";
    s.eVar10="";
    s.eVar11="";
    s.eVar12="";
    s.eVar13="";
    s.eVar14=userObj.fPage|userObj.conPage|userObj.loginConPage?userObj.vType:''; //Visitory Type
    s.eVar15=userObj.fPage|userObj.userIdChange?userObj.userId:'';                     // User ID
    s.eVar16=userObj.conPage?userObj.rType:'';
    s.eVar17="";
    s.eVar18=getUserType();
    s.eVar19=userObj.fPage|userObj.conPage|userObj.aaPage?userObj.regStatus:''; //Registration Status
    s.eVar20=""; // Search
    s.eVar21="";
    s.eVar22="";
    s.eVar23="";                                                      // Refinements
    s.eVar24="D=c43";
    s.eVar25="";
    s.eVar26="";
    s.eVar50=getCiQueryString("AADID");
    //--></script><script type="text/javascript" language="JavaScript" src='http://extras.mnginteractive.com/live/js/omniture/functions.js'></script><script><!--//
    //---------------------------------------------
    /************* DO NOT ALTER ANYTHING BELOW THIS LINE ! **************/
    var s_code=s.t();if(s_code)document.write(s_code)//--></script><script language="JavaScript"><!--
    if(navigator.appVersion.indexOf('MSIE')>=0)document.write(unescape('%3C')+'\!-'+'-')
    //--></script>
    <noscript><img src="http://denverpost.112.2O7.net/b/ss/denverpost/1/H.17--NS/0" height="1" width="1" border="0" alt="" /></noscript>
    <!-- End SiteCatalyst code version: H.17 -->
    </div>
