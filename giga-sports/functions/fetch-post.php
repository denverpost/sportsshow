    <div id="embed-wrapper">
        <div class="hide embed" id="embed-markup">
            <p><strong>Copy the text below to embed:</strong></p>
            <pre>
                &lt;iframe width="654" height="470" src="<?php the_permalink(); ?>?iframe=1" frameborder="0" allowfullscreen seamless scrolling="no">&lt;/iframe>
            </pre>
        </div>
    </div>
<?php 
$link = get_permalink($post->ID); 
$args  = array( 'postid' => $post->ID, 'width' => 940, 'height' => 530, 'hide_href' => false, 'exclude_video' => false, 'imglink' => false, 'imgnocontainer' => true, 'resizer' => '940x529' );
$image = get_obox_media($args);
global $post;
?>

<script>
// Make the post category/ies available via javascript
var the_cats = [];
<?php foreach ( wp_get_post_categories($post->ID) as $item ): ?>
the_cats.push(<?php echo $item; ?>);
<?php endforeach; ?>
</script>

<?php if($image !="") : ?> 
	<div class="post-image fitvid black"> 
<?php if ( $iframe != '' ): ?>
<link rel='stylesheet' id='Oxygen-css'  href='http://fonts.googleapis.com/css?family=Oxygen%3A400%2C300%2C700&#038;subset=latin&#038;ver=4.0' type='text/css' media='all' />
<style type="text/css">
/* Iframeable player-specific styles */
body { margin-left: 0; }
body #playerContainer .oo_promo div.oo_start_button 
{
    bottom: 5%;
    left: 5%;
    background-color: #FFF;
    width: 60px;
    height: 60px;
}
#social-overlay
{
    float: right;
    z-index: 15000; 
    margin-right: 20px;
    margin-top: -12px;
}
ul.post-meta-social-links li
{
    list-style-type: none;
    float: right;
    width: 40px;
    background-color: black;
    padding: 0 2px;
}
ul.post-meta-social-links li img
{
    height: 40px;
    width: 40px;
}
ul.post-meta-social-links li:hover
{
    background-color: #ea212d;
}
.articletools { height:25px; }

/* Headline styles */
#interface
{
    width: 100%;
    padding: 3px 0 12px 12px;
    min-height: 56px;
}
h2
{
    margin: 0;
    width: 70%;
    font-family: Oxygen;
    font-weight: normal;
    font-size: 1.3em;
    line-height: 1.2em;
}
h2 a
{
    color: white;
    text-decoration: none;
}
h2 a:hover
{
    color: #ea212d;
}
.black
{
    background-color: black;
    width: 99%;
}
#playerContainer { border-left: 3px solid black; }
.ndn_playerOptions { color:white; }

/*  We've got two sizes of players we iframe in: 480px, used on homepages and section fronts,
    and 654px, used in articles.
*/
@media only screen and (max-width: 500px)
{
    #interface h2 
    { 
        width: 300px; 
        margin-top: 5px;
        font-size: 1em;
        line-height: 1.1em;
    }
    .black { width: 98%; }
    #playerContainer { border-left: 2px solid black; }
}

/*  We've got two sizes of players we iframe in: 480px, used on homepages and section fronts,
    and 654px, used in articles.
*/
@media only screen and (max-width: 500px)
{
    #interface h2
    {
        width: 300px;
        margin-top: 5px;
        font-size: 1em;
        line-height: 1.1em;
    }
    .black { width: 98%; }
    #playerContainer { border-left: 2px solid black; }
}
@media only screen and (min-width: 700px) and (max-width: 800px)
{
    #interface h2
    {
        width: 100%;
    }
    #playerContainer { border-left: 6px solid black; }
    body { margin: 0; padding: 0; }
}
</style>

<!-- We do this to disable autoplaying of video on iframe page load. -->
<script>
function load_video_iframe(param) {}
var player_ratio = .59;
</script>
<?php
$image = str_replace('load_video', 'load_video_iframe', $image);
// AUTOPLAY?
if ( $autoplay != '' ):
    $image = str_replace("data-config-widget-id='2'", "data-config-widget-id='1'", $image);
    $image = str_replace('window.player = ', 'config.autoplay = true; window.player = ', $image);
else:
    $image = str_replace("data-config-widget-id='2'", "data-config-widget-id='1'", $image);
endif;
echo $image;
?>
            <div id="interface">
                <div id="social-overlay">
<?php 
// Implement the html5-iframe social buttons 

// Ugh, hard-coded ***HC***
// This is called from within a function, so WP functions such as get_theme_root
// and get_template_directory aren't available.
require('./wp-content/themes/giga-sports/social.php'); 
?>
                </div>

                <h2 class="post-title" id="headline"><a href="<?php echo $link; ?>" target="_parent" id="headline-link"><strong>SPORTS SHOW: </strong> <?php the_title(); ?></a></h2>

            </div>
<?php else: ?>
		 <?php echo $image; ?>
<?php endif; ?>
<?php if ( $iframe == '' ): ?>
            <div id="social-player" class="social-insideplayer social-inplayer articletools fb">
<?php include(get_theme_root() . '/' . get_template() . '/social-include.php'); ?>
<?php endif; ?>
    </div>
<?php endif; ?>

<ul class="double-column clearfix">
    <?php if (have_posts()) :
        global $show_author, $post;
        $show_author = 1;
        while (have_posts()) : the_post(); setup_postdata($post); ?>
<?php if ( $iframe == '' ): ?>
        <li id="left-column">
        	<!--Show Title -->
            <h2 class="post-title" id="headline"><a href="<?php echo $link; ?>" target="_parent"><?php the_title(); ?></a></h2>
    <div class="social-wrapper" id="social-tools">
    <?php require('./wp-content/themes/giga-sports/social.php'); ?>
    </div>
           
            <!--Begin Content -->
            <div class="copy clearfix" id="blog-content">
                 <?php the_content(""); ?>
                 <?php wp_link_pages( $args ); ?>
            </div>
            <?php if(comments_open($post->ID)){comments_template();}?>
        </li>
<?php endif; // Close iframe-specific logic ?>
        
    <?php endwhile;
    else :
        ocmx_no_posts();
    endif; ?> 
	<?php get_sidebar(); ?>
</ul>
<?php if ( $iframe == '' ): ?>
            <!-- ##### Begin Outbrain Test ##### -->

            <script>
            var dpcanonical = false;
            var dplinks = document.getElementsByTagName("link");
            for (var i = 0; i < dplinks.length; i++) {
                if (dplinks[i].getAttribute("rel") == "canonical") {
                    var matchHref = dplinks[i].getAttribute("href");
                    matchHrefName = matchHref.split('.');
                    dpcanonical = ( matchHrefName.indexOf('denverpost') > -1 ) ? matchHref : false;
                }
            }
            if (!dpcanonical) {
                dpcanonical = document.URL;
            }
            document.write('<div class="OUTBRAIN" data-src="' + dpcanonical + '" data-widget-id="AR_1" data-ob-template="DenverPost"></div>');
            </script>

            <script>
                if (typeof OBR === 'undefined') {
                    var head = document.getElementsByTagName("head")[0];
                    script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.async = 'async';
                    script.src = 'http://widgets.outbrain.com/outbrain.js';
                    head.appendChild(script);
                }
            </script>

            <!-- ##### End Outbrain Test ##### -->
<?php endif; ?>
