	</div>
<?php if ( $iframe == '' ): ?>
<div id="footer" class="clearfix">
	<ul class="footer-two-column clearfix">
		<?php if (function_exists('dynamic_sidebar')) :
			dynamic_sidebar('footer');
		endif; ?>
	</ul>
	<div class="footer-text">
		<div id="footer-navigation-container">
			<?php if (function_exists("wp_nav_menu")) :	
				wp_nav_menu(array(
					'menu' => 'Gigawatt Footer Nav',
					'menu_id' => 'footer-nav',
					'menu_class' => 'clearfix',
					'sort_column' 	=> 'menu_order',
					'theme_location' => 'secondary',
					'container' => 'ul',
					'fallback_cb' => 'ocmx_fallback_secondary')
					);
				endif; ?>
			</div>
		<p><?php echo stripslashes(get_option("ocmx_custom_footer")); ?></p>
		
		<?php if(get_option("ocmx_logo_hide") != "true") : ?>
			<div class="obox-credit">
			</div>
		<?php endif; ?>
	</div>
</div>
<?php if ( is_home() ): ?>
<div id='div_cube1' class='ad'>
<script type='text/javascript'>
googletag.defineSlot('/8013/denverpost.com/news', [300,250], 'div_cube1').setTargeting('kv', 'dptv').setTargeting('POS', 'Cube1_RRail_ATF').addService

(googletag.pubads());
googletag.pubads().enableSyncRendering();
googletag.enableServices();
googletag.display('div_cube1');

</script>
</div>
<?php endif; ?>
<div id="template-directory" class="no_display"><?php echo bloginfo("template_directory"); ?></div>
<div id="dfmFooter" style=""><div role="contentinfo"><a href="http://www.denverpost.com" style="display:block;" id="dfmFooterLogo"><img src="http://local.denverpost.com/assets/logo-small.png" id="footerLogo"></a></div> <ul class="dfm-share-widget"><li><a rel="tooltip" data-placement="top" data-original-title="Follow us on facebook" class="dfm_fc-webicon" href="https://www.facebook.com/denverpost" style="background: url('http://local.denverpost.com/common/dfm/assets/img/webicons/fc-webicon-facebook-m.png');">facebook</a></li><li><a rel="tooltip" data-placement="top" data-original-title="Follow us on twitter" class="dfm_fc-webicon" href="https://twitter.com/denverpost" style="background: url('http://local.denverpost.com/common/dfm/assets/img/webicons/fc-webicon-twitter-m.png');">twitter</a></li><li><a rel="tooltip" data-placement="top" data-original-title="Follow us on googlePlus" class="dfm_fc-webicon" href="https://plus.google.com/+DenverPost/posts" style="background: url('http://local.denverpost.com/common/dfm/assets/img/webicons/fc-webicon-googlePlus-m.png');">googlePlus</a></li><li><a rel="tooltip" data-placement="top" data-original-title="Follow us on rss" class="dfm_fc-webicon" href="http://www.denverpost.com/webfeeds" style="background: url('http://local.denverpost.com/common/dfm/assets/img/webicons/fc-webicon-rss-m.png');">rss</a></li></ul><!-- .dfm-share-widget --><div id="dfmEndFooterLinks"><ul><li id="dfmCopyText">Copyright © 2014-2015 The Denver Post</li><li><a href="http://www.denverpost.com/portlet/layout/html/copyright/copyright.jsp?siteId=36">Copyright</a></li><li><a href="http://www.denverpost.com/privacypolicy">Privacy Policy</a></li><li><a href="http://www.denverpost.com/sitemap">Site Map</a></li><li><a href="http://www.digitalfirstmedia.com/">Corporate</a></li><li><a href="http://www.denverpost.com/contactus">Contact Us</a></li></ul></div></div>
<?php wp_footer(); ?>
<?php 
	if(get_option("ocmx_googleAnalytics")) :
		echo stripslashes(get_option("ocmx_googleAnalytics"));
	endif;
?>
<script>
  jQuery(document).ready(function(){
	jQuery(".fitvid").fitVids();
  });
</script>
<?php endif; // Close iframe-specific logic ?>
</body>
</html>
