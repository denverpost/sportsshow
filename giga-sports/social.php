            <div class="articletools fb">
<?php if ( $iframe == '' ): ?>
<?php
    $link = wp_get_shortlink();
?>
<!--
                <div class="fb-like" data-href="<?php echo $link; ?>" data-send="false" data-layout="button_count" data-width="100" data-show-faces="false"></div>
-->
                <fb:like send="true" layout="button_count" width="50" show_faces="false" font="arial"></fb:like>
            </div>
            <div class="articletools twt">
                <script src="http://platform.twitter.com/widgets.js" type="text/javascript"></script>
                <a href="http://twitter.com/share" class="twitter-share-button"
                     data-url="<?php echo $link; ?>"
                     data-via="thesportsshow"
                     data-related="WoodyPaige,dpostsports"
                     data-counturl="<?php the_permalink(); ?>"
                     data-text="<?php the_title(); ?>:"
                     data-count="horizontal">Tweet</a>
            </div>
<?php
    else:
    include('social-include.php');
    endif; 
?>
