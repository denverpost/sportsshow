<?php  global $woocommerce; ?>
<?php $iframe = get_query_var('iframe'); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# video: http://ogp.me/ns/video#">
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta name="google-site-verification" content="uxhoEE2xNKcBhEFrDS6PQ3gZXlwIFvY91KWBwezbghs" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <!--Get Obox SEO -->
<?php if(get_option("ocmx_seo") == "yes") {
	echo ocmx_site_title();
	echo ocmx_meta_description();
	echo ocmx_meta_keywords();
} else { ?>
    <title><?php
	global $page, $paged;
	wp_title( '|', true, 'right' );
	bloginfo( 'name' );
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) )
		echo " | $site_description";
	if ( $paged >= 2 || $page >= 2 )
		echo ' | ' . sprintf( __( 'Page %s', 'ocmx' ), max( $paged, $page ) );

function fix_description($text)
{
    // Clean up a blob of text so that it's suitable for including in a meta description.
    $text = strip_tags($text);
    $text = str_replace('"', "'", $text);
    return str_replace("\n", " ", $text);
}
?>
</title>
<?php } ?>

    <!-- Setup OpenGraph support-->
<?php if(get_option("ocmx_open_graph") !="yes") {
	$default_thumb = get_option('ocmx_site_thumbnail');
	$fb_image = get_fbimage();
    $fb_image = get_post_meta($post->ID, 'preview_image_url');
    if ( is_array($fb_image) ) $fb_image = $fb_image[0];
    $fb_image_secure = str_replace('http:', 'https:', $fb_image);
    $video_id = get_post_meta($post->ID, 'video_id');
    if ( is_array($video_id) ) $video_id = $video_id[0];
?>

    <!-- Facebook meta -->
    <meta property="fb:app_id" content="105517551922"/>
<?php	if(is_home()) : ?>
    <meta property="og:title" content="<?php bloginfo('name'); ?>"/>
    <meta property="og:description" content="<?php bloginfo('description'); ?>"/>
    <meta property="og:url" content="<?php echo home_url(); ?>"/>
    <meta property="og:image" content="<?php if(isset($default_thumb) && $default_thumb !==""){echo $default_thumb; } else {echo $fb_image;}?>"/>
    <meta property="og:type" content="video"/>
    <meta property="og:site_name" content="<?php bloginfo('name'); ?> "/>
    <meta name="twitter:image" value="<?php get_template_directory_uri(); ?>/images/logo.jpg" />
    <meta name="twitter:description" value="" />
    <meta name="twitter:url" value="<?php echo get_site_url(); ?>" />
    <meta name="twitter:title" value="<?php bloginfo('name'); ?>" />
	<?php else : ?>
    <meta property="og:type" content="video.other" />
<!--
    <meta property="og:video" content="http://launch.newsinc.com/embed.html?type=VideoPlayer/Single&widgetId=1&trackingGroup=90115&siteSection=denverpost&videoId=<?php echo $video_id; ?>&playlistId=18474" />
    <meta property="og:video:secure_url" content="https://launch.newsinc.com/embed.html?type=VideoPlayer/Single&widgetId=1&trackingGroup=90115&siteSection=denverpost&videoId=<?php echo $video_id; ?>&playlistId=18474" />
    <meta property="og:video:type" content="application/x-shockwave-flash" />
    <meta property="og:video:width" content="654" />
    <meta property="og:video:height" content="368" />
-->
    <meta property="og:title" content="<?php the_title(); ?>" />
    <meta property="og:url" content="<?php the_permalink(); ?> "/>
    <meta property="og:image" content="<?php if($fb_image ==""){echo $default_thumb;} elseif ( function_exists('the_post_thumbnail_url') ) { echo the_post_thumbnail_url(); } else {echo $fb_image;} ?> "/>
    <meta property="og:site_name" content="<?php bloginfo('name'); ?> "/>
    <meta property="og:description" content="<?php echo fix_description($post->post_excerpt); ?> "/>
    <meta property="og:image:secure_url" content="<?php if($fb_image ==""){echo $default_thumb;} else {echo $fb_image_secure;} ?> "/>
    <meta name="twitter:description" value="<?php echo fix_description($post->post_excerpt); ?>" />
    <meta name="twitter:url" value="<?php the_permalink(); ?>" />
    <meta name="twitter:title" value="<?php the_title(); ?>" />
    <?php
    $HTTPS_URL = str_replace('http:', 'https:', get_permalink());
    if ( strpos('?', $HTTPS_URL) > 0 ) $HTTPS_URL .= '&amp;'; else $HTTPS_URL .= '?';
    ?>
    <meta name="twitter:player" content="https://launch.newsinc.com/embed.html?type=VideoPlayer/Single&widgetId=1&trackingGroup=90115&siteSection=denverpost&videoId=<?php echo $video_id; ?>&playlistId=18474" />
<!--
    <meta name="twitter:player:stream" content="https://content-mp4.newsinc.com/mp4/484/<?php echo $video_id; ?>/28967793.mp4" />
    <meta name="twitter:player:stream:content_type" content="video/mp4" />
-->
    <meta name="twitter:player:width" value="600" />
    <meta name="twitter:player:height" value="380" />
    <meta name="twitter:image" value="<?php if ( function_exists('the_post_thumbnail_url') ) { echo the_post_thumbnail_url(); } else echo $fb_image; ?>" />
    <?php endif;
}?>


    <!-- Twitter Card -->
    <meta name="twitter:card" value="player" />
    <meta name="twitter:site" value="@thesportsshow" />
    <meta name="twitter:domain" value="<?php echo get_site_url(); ?>" />
    <meta name="twitter:creator" content="@thesportsshow" /> 
    <!-- Optional Twitter Fields -->
    <meta name="twitter:app:name:iphone" value="Denver Post">
    <meta name="twitter:app:name:ipad" value="Denver Post">
    <meta name="twitter:app:name:googleplay" value="The Denver Post">
    <meta name="twitter:app:id:iphone" value="id375264133">
    <meta name="twitter:app:id:ipad" value="id409389010">
    <meta name="twitter:app:id:googleplay" value="com.ap.denverpost" />

    <!-- Begin Styling -->
<?php if(get_option("ocmx_custom_favicon") != "") : ?>
    <link href="<?php echo get_option("ocmx_custom_favicon"); ?>" rel="icon" type="image/png" />
<?php endif; ?>

    <?php if ( $iframe == '' ): ?>
    <link href="<?php bloginfo('template_directory'); ?>/style.css" rel="stylesheet" type="text/css" />
    <link href="<?php bloginfo('template_directory'); ?>/responsive.css" rel="stylesheet" type="text/css" />

<?php if(get_option("ocmx_theme_style") !="") :
	echo theme_colour_styles();
else : ?>

    <link href="<?php bloginfo('template_directory'); ?>/color-styles/light/style.css" rel="stylesheet" type="text/css" />
<?php endif; ?>

<?php if(get_option("ocmx_rss_url")) : ?>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php echo get_option("ocmx_rss_url"); ?>" />
<?php else : ?>
    <link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<?php endif; ?>

    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />

<!--[if lt IE 9]>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/ie.css" media="screen" />
<![endif]-->
    <style type="text/css">
    h1 a
    {
        background: url('<?php echo get_template_directory_uri(); ?>/images/logo-wide.png') no-repeat no-repeat;
    }
    @media only screen and (max-width: 610px) 
    {
        h1 a
        {
            background: url('<?php echo get_template_directory_uri(); ?>/images/logo-small.png') no-repeat no-repeat;
        }
    }
    h1 a span
    {
        background: url('<?php echo get_template_directory_uri(); ?>/images/logo-tagline.jpg') no-repeat top right;
    }
    #banner
    {
        background: #ea212d url('<?php echo get_template_directory_uri(); ?>/images/denverpost.png') no-repeat center center;
    }
    .ndn_embed
    {
        width: 100%;
    }
    h2 a { color: #EA212D; }
    h2 a:hover { color: #EA212D!important; }
    #widget-block .three-column h2.post-title, #widget-block h2.post-title
    {
        padding-bottom: 20px;
        margin-bottom: 5px;
        border-bottom: 1px solid #bbb;
    }
    </style>

<?php wp_head(); ?>
    <?php else: ?>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <style type="text/css">
/* Embed box styles */
.hide { display: none; }
.show { display: block; }
.embed
{
    padding: 12px 20px;
    margin: auto;
    font-size: 14px;
    width: 80%;
    background-color: white;
}
#embed-wrapper
{
    margin: auto;
    position: absolute;
    top: 50px;
    z-index: 100000;
    width: 100%;
    max-width: 700px;
}
pre {padding: 15px;font-variant: normal; white-space: pre-wrap;white-space: pre-line; word-wrap: break-word;text-align: left; margin: 0px 0px 22px 0px;  font-family: Consolas, Monaco, 'Courier New', Courier, monospace; background-color: #f0f0f0;
font-size: 12px; line-height: 1.3em; overflow-x: auto; overflow-y: auto; color: #000000; font-weight: normal; font-style: normal; letter-spacing: normal; orphans: 2; text-indent: 0px; text-transform: none; widows: 2; word-spacing: 0px; padding: 11px; border: 1px solid #dadada;}
    </style>
    <?php endif; // Close iframe-specific logic ?>

    <script type='text/javascript'>
    (function() {
    var useSSL = 'https:' == document.location.protocol;
    var src = (useSSL ? 'https:' : 'http:') +
    '//www.googletagservices.com/tag/js/gpt.js';
    document.write('<scr' + 'ipt src="' + src + '"></scr' + 'ipt>');
    })();
    </script>
</head>

<body <?php body_class(''); ?>>
<script src="https://player.ooyala.com/v3/df3e4364f37c45d09283f7d098c03fce?platform=html5-fallback"></script>
<script type="text/javascript" src="http://launch.newsinc.com/js/embed.js" id="_nw2e-js"></script>
<?php if ( $iframe == '' ): ?>
<?php include('omniture.php'); ?>
<?php endif; // Close iframe-specific logic ?>

<?php include('social_header.php'); ?>

<?php if ( $iframe == '' ): ?>
    <div id="banner" onClick="document.location='http://www.denverpost.com/';"><a href="http://www.denverpost.com/">The Denver Post</a></div>
<?php endif; // Close iframe-specific logic ?>
    <?php if ( is_home() ): ?>
    <div id='div_top_leaderboard' class='ad'>
    <script type='text/javascript'>
    googletag.defineSlot('/8013/denverpost.com/sports', [728,90], 'div_top_leaderboard').setTargeting('kv', 'sportsshow').setTargeting('POS', 'top_leaderboard').addService

    (googletag.pubads());
    googletag.pubads().enableSyncRendering();
    googletag.enableServices();
    googletag.display('div_top_leaderboard');
    </script>
    </div>
    <?php endif; ?>
	<div id="header-container">
		<?php 
		$headercart = get_option("ocmx_header_cart"); 
		if(isset($headercart) && $headercart == "yes") : 
			get_template_part("/functions/header-cart");
		endif; 
		?>

<?php if ( $iframe == '' ): ?>
<script>
// Check to make sure we're not being iframe'd.
 if (top !== self) 
 {
    var denverposturl = self.document.location.toString();
    if ( denverposturl.indexOf("sportsshow.denverpost.com") > 0 && document.referrer.indexOf("denverpost.com") > 0 )
    {
            self.location.replace(self.location + "?iframe=1");
    }
 }
</script>
		<div id="header" class="clearfix">
			<div class="logo">
				<h1>
					<a href="http://www.denverpost.com/sportsshow">
						<?php if(get_option("ocmx_custom_logo")) : ?>
							<img src="<?php echo get_option("ocmx_custom_logo"); ?>" alt="<?php bloginfo('name'); ?>" />
						<?php else : ?>
							<?php bloginfo('name'); ?>
						<?php endif; ?>
					</a>
				</h1>
			</div>

			<div id="navigation-container">
				<a id="menu-drop-button" href="#"></a>
				<?php if (function_exists("wp_nav_menu")) :
					wp_nav_menu(array(
						'menu' => 'Gigawatt Header Nav',
						'menu_id' => 'nav',
						'menu_class' => 'clearfix',
						'sort_column' 	=> 'menu_order',
						'theme_location' => 'primary',
						'container' => 'ul',
						'fallback_cb' => 'ocmx_fallback_primary')
					);
				endif; ?>
			</div>
		</div>
	</div>
    <?php endif; // Close iframe-specific logic ?>

<div id="content-container" class="clearfix">
