<?php  global $themename, $input_prefix;

/*****************/
/* Theme Details */

$themename = "Gigawatt";
$themeid = "gigawatt";
$productid = "1460";
$presstrendsid = "d240ke2rfe2mszfboyodf564x1no5p9x9";

/**********************/
/* Include OCMX files */
$include_folders = array("/ocmx/includes/", "/ocmx/theme-setup/", "/ocmx/widgets/", "/ocmx/front-end/", "/ajax/", "/ocmx/interface/");

// This is a hack, to avoid the "headers already sent by...." error which pops up when you call pages directly from wp-admin/ like edit.php for example
include_once (get_template_directory()."/ocmx/folder-class.php");
include_once (get_template_directory()."/ocmx/load-includes.php");
include_once (get_template_directory()."/ocmx/custom.php");
include_once (get_template_directory()."/ocmx/seo-post-custom.php");

/**********************/
/* "Hook" up the OCMX */

update_option("ocmx_font_support", true);
add_action('init', 'ocmx_add_scripts');

function add_widgetized_pages(){
	global $wpdb;
	$get_widget_pages = $wpdb->get_results("SELECT * FROM ".$wpdb->postmeta." WHERE `meta_key` = '_wp_page_template' AND  `meta_value` = 'widget-page.php'");
	foreach($get_widget_pages as $pages) :
		$post = get_post($pages->post_id);
		register_sidebar(array("name" => $post->post_title." Slider Widget", "description" => "Place all 'Home Page Widgets' here."));
		register_sidebar(array("name" => $post->post_title." Body", "description" => "Place all 'Home Page Widgets' here.", "before_title" => '<h4 class="widgettitle">', "after_title" => '</h4><div class="content">', 'before_widget' => '<li id="%1$s" class="widget %2$s">', 'after_widget' => '</div></li>'));
	endforeach;
}
add_action("init", "add_widgetized_pages");

/*********************/
/* Load Localization */
load_theme_textdomain('ocmx', get_template_directory() . '/lang');

/***********************/
/* Add OCMX Menu Items */

add_action('admin_menu', 'ocmx_add_admin');
function ocmx_add_admin() {
	global $wpdb;
	add_object_page("Theme Options", "Theme Options", 'edit_themes', basename(__FILE__), '', 'http://obox-design.com/images/ocmx-favicon.png');
	add_submenu_page(basename(__FILE__), "General Options", "General", "administrator", basename(__FILE__), 'ocmx_general_options');
	add_submenu_page(basename(__FILE__), "Customize", "Customize", "edit_theme_options", "customize.php");
	add_submenu_page(basename(__FILE__), "Typography", "Typography", "administrator", "ocmx-fonts", 'ocmx_font_options');
	add_submenu_page(basename(__FILE__), "Adverts", "Adverts", "administrator",  "ocmx-adverts", 'ocmx_advert_options');
	add_submenu_page(basename(__FILE__), "SEO Options", "SEO Options", "administrator", "ocmx-seo", 'ocmx_seo_options');
	add_submenu_page(basename(__FILE__), "Help", "Help", "manage_options", "obox-help", 'ocmx_welcome_page');

};

/*****************/
/* Add Nav Menus */

if (function_exists('register_nav_menus')) :
	register_nav_menus( array(
		'primary' => __('Primary Navigation', '$themename'),
		'secondary' => __('Secondary Navigation', '$themename')
	) );
endif;

/************************************************/
/* Fallback Function for WordPress Custom Menus */

function ocmx_fallback_primary() {
	echo '<ul id="nav" class="clearfix">';
	wp_list_pages('title_li=&');
	echo '</ul>';
}


/************************************************/
/* Fallback Function for WordPress Custom Menus */

function ocmx_fallback_secondary() {
	echo '<ul id="footer-nav" class="clearfix">';
	wp_list_pages('title_li=&');
	echo '</ul>';
}
function obox_check_current_theme() {
	$theme = wp_get_theme();
	if( $theme->stylesheet == 'gigawatt-ecommerce' ) { ?>
		<div class="updated">
			<p><?php _e( '<strong>Important Notice:</strong> With Gigawatt 2.0+ there is no longer any need to use the Gigawatt Ecommerce child theme. Please activate the <strong>Gigawatt</strong> parent theme via <a href="themes.php?s=Gigawatt">Appearance > Themes</a>.', 'ocmx' ); ?></p>
		</div>
	<?php }
}
add_action( 'admin_notices', 'obox_check_current_theme' );

/**************************/
/* WP 3.4 Support         */
add_theme_support('custom-background');
add_theme_support('post-thumbnails');
if ( ! isset( $content_width ) ) $content_width = 980;

add_action( 'after_setup_theme', 'theme_setup' );
function theme_setup() {
  add_image_size('940x529', 940, 529, true);
  add_image_size('220x125', 220, 125, true);
  add_image_size('140x100', 220, 125, true);
  add_image_size('460x259', 460, 259, true);
  add_image_size('550x309', 550, 309, true);
  add_image_size('300x169', 300, 169, true);
  add_image_size('300x180', 300, 180, true);
  add_image_size('940', 940, 9999);
  add_image_size('680', 680, 9999);
  add_image_size('220', 220, 9999);
}
/********************************************************************/
/* Custom fix for iframes with youtube hiding menus and other bits */

function add_video_wmode_transparent($html) {
	if (strpos($html, "<iframe" ) !== false) {
	$search = array('" frameborder="0"', '?hd=1?hd=1');
		$replace = array('?hd=1&wmode=transparent" frameborder="0"', '?hd=1');
		$html = str_replace($search, $replace, $html);
	return $html;
   } else {
		return $html;
   }
}

add_filter('the_excerpt', 'add_video_wmode_transparent', 10);
add_filter('the_content', 'add_video_wmode_transparent', 10);
add_filter('obox_image', 'add_video_wmode_transparent', 10);

/**************************/
/* Facebook Support      */
function get_fbimage() {
	global $post;
	if ( !is_single() ){
		return '';
	}
	$src = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), '', '' );
	$fbimage = null;
	if ( has_post_thumbnail($post->ID) ) {
		$fbimage = $src[0];
	} else {
		global $post, $posts;
		$fbimage = '';
		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i',
		$post->post_content, $matches);
		if(!empty($matches[1]))
			$fbimage = $matches [1] [0];
	}
	if(empty($fbimage)) {
		$fbimage = get_the_post_thumbnail($post->ID);
	}
	return $fbimage;
}

/******************************************************************************/
/* Each theme has their own "No Posts" styling, so it's kept in functions.php */

function ocmx_no_posts(){ ?>
	<li class="post">
		<h2 class="post-title"><?php _e("No Posts Found", 'ocmx'); ?></h2>
		<div class="copy clearfix">
			<?php _e("There are no posts which match your search criterea."); ?>
		</div>
	</li>
<?php
}
/**************************/
/* Set the Excerpt Length */
function obox_excerpt_length($length) {
	return 35;
}
function obox_excerpt_more($more) {
	return '...';
}
add_filter('excerpt_length', 'obox_excerpt_length');
add_filter('excerpt_more', 'obox_excerpt_more');


/************************************/
/* Declare WooCommerce Support */
add_theme_support( 'woocommerce' );

// *******************
//
// GET PARAMS WE USE FOR ADD-ON FUNCTIONALITY
//
// *******************
add_action('init','theme_register_param');
function theme_register_param() 
{ 
    global $wp; 
    $wp->add_query_var('json'); 
    $wp->add_query_var('js'); 
}
