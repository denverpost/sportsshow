<?  if ( $json == '1' ): ?>
{ "items": [ <?php
    if (have_posts()) :
        $post_count = count($posts);
        $post_counter = 0;
        while (have_posts()) :  the_post(); setup_postdata($post);
            $post_counter ++;
            $link = get_permalink($post->ID);
            $video_id = get_post_meta($post->ID, "video_id", true);
            if ( trim($video_id) == '' ) continue;
?>
    {"title": "<?php the_title(); ?>", "video_id": "<?php echo $video_id; ?>", "url": "<?php echo $link; ?>", "description": "<?php echo trim(str_replace(array("\r\n", "\r", "\n"), '', addslashes($post->post_excerpt))); ?>" }
<?php
            if ( $post_counter !== $post_count ) { echo ","; }
        endwhile;
    endif;
?>
]}
<?
elseif ( $js == '1' ):
?>
var video_list = [ <?php
    if (have_posts()) :
        $post_count = count($posts);
        $post_counter = 0;
        while (have_posts()) :  the_post(); setup_postdata($post);
            $post_counter ++;
            $link = get_permalink($post->ID);
            $video_id = get_post_meta($post->ID, "video_id", true);
            if ( trim($video_id) == '' ) continue;
?>
    {"title": "<?php the_title(); ?>", "video_id": "<?php echo $video_id; ?>", "url": "<?php echo $link; ?>", "description": "<?php echo trim(str_replace(array("\r\n", "\r", "\n"), '', addslashes($post->post_excerpt))); ?>" }
<?php
            if ( $post_counter !== $post_count ) { echo ","; }
        endwhile;
    endif;
?>
];
<?php else: ?>
<?php get_header(); ?>
<?php
            if (have_posts()) :
                while (have_posts()) :  the_post(); setup_postdata($post);
?>
        <ul class="single-column">
            <?php
                global $post;
                $link = get_permalink($post->ID);
                $args  = array('postid' => $post->ID, 'width' => 940, 'height' => 529, 'hide_href' => false,  'imglink' => false, 'exclude_video' => '0', 'imgnocontainer' => true, 'resizer' => '940x529');
                $image = get_obox_media($args);
                if($show_images != "on" || $image == "") : $maxlen = 90; else : $maxlen = 75; endif;  ?>
                <li class="column">
<script>
// Make the post category/ies available via javascript
var the_cats = [];
<?php foreach ( wp_get_post_categories($post->ID) as $item ): ?>
the_cats.push(<?php echo $item; ?>);
<?php endforeach; ?>
</script>
    <div id="embed-wrapper">
        <!-- <p>&raquo; <a href="#" onClick="jQuery('#embed-markup').toggleClass('hide'); return false;">Embed this video</a></p> -->
        <div class="hide embed" id="embed-markup">
            <p><strong>Copy the text below to embed:</strong></p>
            <pre>
                &lt;iframe width="654" height="470" src="<?php the_permalink(); ?>?iframe=1" frameborder="0" allowfullscreen seamless scrolling="no">&lt;/iframe>
            </pre>
        </div>
    </div>
                    <?php if($image !="") : ?>
                    <div class="post-image fitvid">
                        <?php echo $image; ?>
                    </div>
                    <?php endif; ?>
                    <?php if($show_dates != false) : ?><h5 class="date"><?php echo date('d M Y', strtotime($post->post_date)); ?></h5><?php endif; ?>
                        <h2 class="post-title" id="headline"><a href="<?php echo $link; ?>"><?php the_title(); ?></a></h2>

                    <?php if($show_excerpts != false) :
                        if($post->post_excerpt != "") :
                            echo "<p>".substr(strip_tags($post->post_excerpt), 0, $maxlen)."...</p>";
                        else :
                            the_content("");
                        endif;
                    endif; ?>
<?php if ( is_home() ): ?>
            <div id="social-player" class="social-inplayer articletools fb">
<?php
    include(get_theme_root() . '/' . get_template() . '/social-include.php');
endif; ?>
                </li>
        </ul>
<?php
                    break;
                endwhile;
            endif;
?>

<ul class="double-cloumn clearfix">
    <li id="left-column">   
        <ul class="blog-main-post-container clearfix">
            <?php if (have_posts()) :
                while (have_posts()) :  the_post(); setup_postdata($post);
                    get_template_part("/functions/fetch-list");
                endwhile;
            else :
                ocmx_no_posts();
            endif; ?>
        </ul>
        <?php motionpic_pagination("clearfix", "pagination clearfix"); ?>
    </li>
    <?php get_sidebar(); ?>
</ul>
<?php get_footer(); ?>
<?php
endif;
