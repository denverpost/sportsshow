<?php

/**
 * This class is for the display of results from tests run by other classes to make sure they are working
 *
 * @author Tim Kerstiens
 */
class TestResults {
	/**
	 * Holds the results of the test
	 *
	 * @var array $results
	 */
	private $results;
	
	/**
	 * Holds the summary data for the results
	 *
	 * @var array $summary
	 */
	private $summary;
	
	/**
	 * Sole constructor.  Initilizes the class and saves the results to the object
	 *
	 * @param array $results An array of test results.  The key should be the name of the test and the value should be true or false representing 
	 *     pass or fail, respectively.  This also creates the summary data for the results.
	 */
	public function __construct($results) {
		$this->results = $results;
		
		$pass = 0;
		$fail = 0;
		$other = 0;
		$total = sizeof($this->results);
		
		foreach($this->results as $result) {
			if(!is_bool($result)) {
				$other++;
			} else {
				if($result) {
					$pass++;
				} else {
					$fail++;
				}
			}
		}
		
		$this->summary = array(
			'pass' => $pass,
			'fail' => $fail,
			'other' => $other,
			'total' => $total
		);
	}
	
	/**
	 * Displays the test results of the object using the static method dsiplayResults
	 */
	public function display() {
		TestResults::displayResults($this->results);
	}
	
	/**
	 * Displays the summary data for the results of the object.  Summary fields are Pass, Fail, Other, and Total
	 */
	public function displaySummary() {
		echo 'TEST RESULTS SUMMARY' . PHP_EOL;
		echo 'PASS: ' . $this->summary['pass'] . PHP_EOL;
		echo 'FAIL: ' . $this->summary['fail'] . PHP_EOL;
		echo 'OTHER: ' . $this->summary['other'] . PHP_EOL;
		echo 'TOTAL: ' . $this->summary['total'] . PHP_EOL;
	}
	
	/**
	 * Displays the test results in a visually acceptable manner
	 *
	 * @param array $results An array of test results.  The key should be the name of the test and the value should be true or false representing
	 *     pass or fail, respectively.
	 */
	public static function displayResults($results) {
		$successMessage = "\033[0;30m\033[42mSuccess\033[0m";
        $failureMessage = "\033[0;30m\033[41mFailure\033[0m";
		foreach($results as $test => $result) {
			if(!is_bool($result)) {
				echo " $test: $result " . PHP_EOL;
			} else {
				$test = str_pad($test, 50);
				echo "\033[;30m\033[";
				if($result) {
					echo "42m $test Success";
				} else {
					echo "41m $test Failure";
				}
				echo " \033[0m" . PHP_EOL;
			}
		}
	}
	
	/**
	 * This method is for testing itself, but since all this class does is output data it must be checked manually.  Can also be read through to see 
	 *     how to use this class
	 *
	 * @param array $arguments The command line arguments passed into php
	 */
	public static function main($arguments) {
		$results = array(
			'Test1' => true,
			'Test2' => true,
			'This Test' => 'Unable to run due to invalid file input',
			'Test3' => true,
			'Test4' => false,
			'Test5' => true,
			'Test6' => false
		);
		
		TestResults::displayResults($results);
		echo PHP_EOL;
		$results = new TestResults($results);
		$results->display();
		$results->displaySummary();
	}
}

if(isset($_SERVER['SCRIPT_FILENAME']) && $_SERVER['SCRIPT_FILENAME'] == 'TestResults.php') {
	TestResults::main($argv);
}
