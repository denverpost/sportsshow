<?php
/**
 * Plugin Name: Ooyala Autopost
 * Plugin URI: 
 * Description: Every time an Ooyala video from a particular label comes in, create a blog post for it.
 * Version: 0.1
 * Author: Joe Murphy
 * Author URI: 
 * License: Apache-2
 */

include('ooint/OoyalaInterface.php');
class Article
{
    // Methods for handling Denver Post article objects.
    var $url;
    var $title;

    function __construct()
    {
    }

    public function set_url($url)
    {
        // Set the value of the test var.
        return $this->url = $url;
    }

    public function lookup_title()
    {
        // Pull an article, take its pieces. 
        // Sometimes we just want the title, sometimes we want more.

        $content = $this->get_content($this->url);
        preg_match('/<h1 id="articleTitle" class="articleTitle">([^<]+)<\/h1>/', $content, $matches);
        $article['title'] = $matches[1];
        return $matches[1];
    }

    private function get_content($url)
    {
        $url=str_replace('&amp;','&',$url);
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $markup = curl_exec($ch);
        curl_close($ch);
        return $markup;
    }
}

class Ooyala
{

    var $json;
    var $url;
    var $test;
    var $path_prefix;

    function __construct()
    {
        global $wpdb;
        $this->url = '';
        $this->test = true;
        $this->path_prefix = '';
        $this->label_id = '';
        $this->api = '';
        $this->api_keys = Array(
            'public' => '',
            'private' => '');
        if ( function_exists('plugin_dir_path') ):
            $this->path_prefix = plugin_dir_path( __FILE__ );
        endif;

        if ( !class_exists('Easy_CF') ):
            require_once( WP_PLUGIN_DIR . '/easy-custom-fields/easy-custom-fields.php' );
        endif;
        //if ( class_exists('Easy_CF') ):
            $test = $wpdb->get_results( "SELECT meta_key FROM wp_postmeta where meta_key='video_id'" );
            if ( $wpdb->num_rows == 0 ):
                // Create the custom fields
                $field_data = array (
                    'Ooyala' => array (             // unique group id
                        'fields' => array(             // array "fields" with field definitions
                            'video_id'  => array(),      // globally unique field id
                            'preview_image_url'  => array(),
                            'duration'  => array()
                        ),
                    ),
                );
                $easy_cf = new Easy_CF($field_data);
            endif;
        //endif;
    }

    public function set_test($test)
    {
        // Set the value of the test var.
        return $this->test = $test;
    }

    public function setup_api()
    {
        // Connect to Ooyala's API.
        // Assumes API key values have already been set.
        if ( $this->api_keys['public'] == '' ):
            // ^^^ Raise error, exit
            return false;
        endif;
        if ( $this->api_keys['private'] == '' ):
            // ^^^ Raise error, exit
            return false;
        endif;

        $this->api = new OoyalaInterface($this->api_keys['public'], $this->api_keys['private']);
    }

    public function get_json()
    {
        // Get the JSON
        if ( $this->test == true ):
            $this->url = $this->path_prefix . 'updates.json';
        endif;

        $this->json = file_get_contents($this->url);
        return $this->json;
    }

    public function write_json()
    {
        if ( $this->json === false ):
            $this->json = $this->get_json();
        endif;
        if ( $this->json != false ):
            return file_put_contents($this->path_prefix . 'updates.json', $this->json);
        endif;
        return false;
    }

    public function parse_json()
    {
        // Take the json in updates.json and pull out the chunk that we want.
        // Will have to build this method out if we want to measure things
        // in addition to the Rockies wins.
        if ( $this->json === false ):
            $this->json = $this->get_json();
        endif;
        $object = json_decode($this->json);

        return $data;
    }

}

class AutoPost
{

    static public function slugify($text)
    { 
        // From http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text))
        {
            return 'n-a';
        }

        return $text;
    }


    static public function fetch_media($file_url, $post_id) 
    {
        /* Import media from url
         *
         * @param string $file_url URL of the existing file from the original site
         * @param int $post_id The post ID of the post to which the imported media is to be attached
         *
         * @return boolean True on success, false on failure
         */
        // From http://nlb-creations.com/2012/09/26/how-to-programmatically-import-media-files-to-wordpress/
        require_once(ABSPATH . 'wp-load.php');
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        global $wpdb;

        if ( !$post_id ) return false;

        //directory to import to    
        $artDir = 'wp-content/uploads/importedmedia/';

        //if the directory doesn't exist, create it 
        //if ( !file_exists(ABSPATH.$artDir) ) mkdir(ABSPATH.$artDir);

        // rename the file... alternatively, you could explode on "/" and keep the original file name
        // Note: This assumer a file extension on the image.
        //$ext = array_pop(explode(".", $file_url));
        //if your post has multiple files, you may need to add a random number to the file name to prevent overwrites
        //$new_filename = 'photo-'.$post_id.".".$ext; 
        $new_filename = 'photo-'.$post_id.'.jpg';

        if (@fclose(@fopen($file_url, "r"))):
            // make sure the file actually exists
            $ch = curl_init($file_url);
            $fp = fopen(ABSPATH.$artDir.$new_filename, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            //file_put_contents(ABSPATH.$artDir.$new_filename, file_get_contents($file_url));

            $siteurl = get_option('siteurl');
            $file_info = getimagesize(ABSPATH.$artDir.$new_filename);

            //create an array of attachment data to insert into wp_posts table
            $artdata = array();
            $artdata = array(
                'post_author' => 1, 
                'post_date' => current_time('mysql'),
                'post_date_gmt' => current_time('mysql'),
                'post_title' => $new_filename, 
                'post_status' => 'inherit',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => sanitize_title_with_dashes(str_replace("_", "-", $new_filename)),
                'post_modified' => current_time('mysql'),
                'post_modified_gmt' => current_time('mysql'),
                'post_parent' => $post_id,
                'post_type' => 'attachment',
                'guid' => $siteurl.'/'.$artDir.$new_filename,
                'post_mime_type' => $file_info['mime'],
                'post_excerpt' => '',
                'post_content' => ''
            );

            $uploads = wp_upload_dir();
            $save_path = $uploads['basedir'].'/importedmedia/'.$new_filename;
            //$save_path = $uploads['basedir'].$new_filename;

            //insert the database record
            $attach_id = wp_insert_attachment( $artdata, $save_path, $post_id );

            //generate metadata and thumbnails
            if ($attach_data = wp_generate_attachment_metadata( $attach_id, $save_path)) {
                wp_update_attachment_metadata($attach_id, $attach_data);
            }

            //optional make it the featured image of the post it's attached to
            $rows_affected = $wpdb->insert($wpdb->prefix.'postmeta', array('post_id' => $post_id, 'meta_key' => '_thumbnail_id', 'meta_value' => $attach_id));
        else: return false;
        endif;

        return true;
    }

}

function ooyala_execute($test=false)
{
    add_action('init', 'ooyala_execute_autopost');
}

function ooyala_execute_autopost($test=false, $label='')
{
    // Max posts per category ingested
    $limit = 4;

    // This gets run on the crontab.
    // GET /v2/labels
    // Each label slug must be the same as the slug of a category in WP.
    $labels = array(
        'colorado-rapids' => '450cdaedeb67472aa9f3ddf2b8a16763',
        'horse-racing' => '696931f5defe4083b65e4c1e3cc3df81',
        'sports-business' => '9579039fe54d4bcf9007a1f662b9f3de',
        'nhl-colorado-avalanche' => '226f051040e9408e855cbd501994b1b1',
        'mlb-colorado-rockies' => 'aee37e9ae4564f82a301d2d9f3f94488',
        'nba-denver-nuggets' => 'b5f48d858b0548ccba0ed1984ae2d1ab',
        'nfl' => '1ba6ad0dffa9481c99f3c624fdf950b5',
        'colorado-rapids' => '0bc5ad7adaff4ddaacba3c1199be5d52',
        'you-gotta-see-this' => '9e4697d95d8748beaa7081aab08d8a33',
        'social-animals' => 'ae66a43e1fd444f5baf6ee007a62e784',
        'pop-a-shot-sports-show' => '7b31aca5b6cb423b95139de446401533',
        'pick-six' => 'e92eefa2ddb044299dae432e3e3554bf',
        'more-with-les' => '95500053b4d747ad84fcbcb14d847899',
        'the-big-stuff' => '709978069d5241689edad78408cd04d4',
        'woodys-world' => '4a6d64811a064843874541c814f3550d',
        'broncos' => '8206796626744d84af3f7b18179abb02',
        'guests' => 'b507170376fe45259bd67f7e0319f6af',
        'sports-show' => 'a876f822226d431793c4bf45cee3eda2',
        'sports-show-mornings' => '3cc70e1fbfe54468b3c78229db710e80',
        'sports' => '7bedaacf886f46a9afe8515dc32321ed',
        //'full-episode' => 'efd523b1e399402d8029873386fc1803',
    );
    $already_executing = get_transient('already_executing');

    // If we're passing a label, we only run this function on the label passed.
    if ( $label != '' ):
        $already_executing = FALSE;
        $limit = 15;
        $new_label[$label] = $labels[$label];
        unset($labels);
        $labels = $new_label;
    endif;

    if ( $already_executing !== FALSE ):
        echo "<h2>Note: Already Executing. Wait up to 60 seconds before trying again.</h2>";
    endif;

    if ( $already_executing == FALSE ):
        // Make sure we don't run this on the crontab more than once at the same time.
        set_transient('already_executing', 1, 100);
        $oo = new Ooyala();
        $oo->api_keys['public'] = 'V5dzkxOmUFf0dFju2v9bPHqRdgjC.MNlZu';
        $oo->api_keys['private'] = trim(file_get_contents(__DIR__ . '/.ooyala_key'));
        $oo->setup_api();
        foreach ( $labels as $key => $value ):
            $category = get_category_by_slug($key);
            if ( $category === false ):
                echo "<strong>Could not find matching category $key, skipping this channel.</strong>";
                continue;
            endif;

            $oo->label_id = $value;
            $items = $oo->api->get('/v2/labels/' . $oo->label_id . '/assets', Array('limit' => $limit));

            foreach ( array_reverse($items->items) as $item ):

                // See if a post already exists with the video id (aka embed_code).
                // We call it video id because embed code, while being the name ooyala
                // uses to describe it, isn't as specific.

                $video_id = $item->embed_code;
                $args = array(
                    'post_status' => array('publish', 'draft', 'auto-draft', 'future'),
                    'numberposts' => 1,
                    'meta_key' => 'video_id',
                    'meta_value' => $video_id);
                if ( $label != '' ) echo "<p>Looking up video id " . $video_id . ".</p>";

                $query = new WP_Query($args);
                if ( count($query->posts) > 0 && $label != '' ) echo "<p>Found a post with that video_id, " . $query->post->ID . ".</p>";

                if ( count($query->posts) == 0 ):
                    $autopost = new AutoPost();
                    $post_id = -1;
                    $author_id = 1;
                    $slug = $autopost->slugify($item->name);
                    $title = $item->name;

                    // Auto-publish up-to-date stories
                    $post_status = 'draft';
                    if ( $key == 'up-to-date' ) $post_status = 'published';

                    $post_id = wp_insert_post(
                        array(
                            'post_author' => $author_id,
                            'post_name' => $slug,
                            'post_title' => $title,
                            'post_content' => $item->description,
                            'post_excerpt' => $item->description,
                            'post_status' => $post_status,
                            'post_type' => 'post',
                            'post_category' => array($category->term_id)
                        )
                    );
                    update_post_meta($post_id, 'video_id', $video_id);
                    update_post_meta($post_id, 'preview_image_url', $item->preview_image_url);

                    if ( $label != '' ) echo "<p>Published post '" . $title . "'.</p>";

                    // Now upload the thumbnail image to the system and associate
                    $autopost->fetch_media($item->preview_image_url, $post_id);
                    $cachebuster = '201509';
                    $main_video = "
                        <div id='playerContainer'></div>
                        <script src='" . get_template_directory_uri() . "/scripts/ooyala.js?" . $cachebuster . "'></script>
                        <script>
                            OO.ready(function() { window.player = OO.Player.create('playerContainer', '" . $video_id . "', config);
                            load_video('" . $video_id . "'); });
                        </script>";
                    update_post_meta($post_id, 'main_video', $main_video);


                endif;
            endforeach;
        endforeach;
    endif;
}

// *******************
//
// CRONTAB
//
// *******************

// We do this to get the Ooyala video-posts published every 60 minutes
// On an early action hook, check if the hook is scheduled - if not, schedule it.
add_filter( 'cron_schedules', 'ooyala_add_custom_cron_schedule' );
function ooyala_add_custom_cron_schedule( $schedules ) 
{
    $schedules['twominute'] = array(
        'interval' => 120,
        'display'  => __( 'Every 2 Minutes' ),
    );
    $schedules['fiveminute'] = array(
        'interval' => 300,
        'display'  => __( 'Every 5 Minutes' ),
    );
    $schedules['30min'] = array(
        'interval' => 1800,
        'display'  => __( 'Every 30 Minutes' ),
    );
    return $schedules;
}

add_action( 'wp', 'ooyala_crontab_setup' );
function ooyala_crontab_setup() 
{
    if ( !wp_next_scheduled( 'autopost_event' ) ):
        $return = wp_schedule_event(time(), 'twominute', 'autopost_event');
    endif;
}
add_action( 'autopost_event', 'ooyala_execute_autopost' );
//wp_clear_scheduled_hook('autopost_event');


// *******************
//
// WP-ADMIN OPTIONS PAGE
//
// *******************
class debug_options_page
{
    function __construct()
    {
        $this->slug = 'ooyala-debug';
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    function admin_menu() { add_options_page('Page Title', 'Ooyala: Debug', 'manage_options', $this->slug, array($this, 'settings_page') ); }

    function settings_page()
    {
        echo '
        <h1>Ooyala Debug</h1>
        <p>Figure out where a video you need published is, and publish it if need be.</p>
        <form method="GET" action="./options-general.php">
            <p><label for="video_id">Ooyala Video ID:</label><input type="text" id="video_id" name="video_id" maxlength="50" value="" /></p>
            <input type="hidden" name="page" value="ooyala-debug" />
            <input type="submit" value="Look Up Video ID">
        </form>
        ';
        if ( isset($_GET['video_id']) ):
            $video_id = trim(htmlspecialchars($_GET['video_id']));
            $args = array( 'numberposts' => 1, 'meta_key' => 'video_id', 'meta_value' => $video_id, 'post_status' => array('publish', 'draft', 'auto-draft', 'future'));
            $query = new WP_Query($args);
            if ( $query->have_posts() == FALSE ):
                echo '<p style="color:red">Video ' . $video_id . ' was not found in any existing posts. Attempting to import it now.</p>';
                // Get the ooyala video information from ooyala
                $oo = new Ooyala();
                $oo->api_keys['public'] = 'V5dzkxOmUFf0dFju2v9bPHqRdgjC.MNlZu';
                $oo->api_keys['private'] = trim(file_get_contents(__DIR__ . '/.ooyala_key'));
                $oo->setup_api();
                $item = $oo->api->get('/v2/assets/' . $video_id);
                var_dump($item);

                $autopost = new AutoPost();
                $post_id = -1;
                $author_id = 1;
                $slug = $autopost->slugify($item->name);
                $post_id = wp_insert_post(
                    array(
                        'post_author' => $author_id,
                        'post_name' => $item->name,
                        'post_title' => $item->name,
                        'post_content' => $item->description,
                        'post_excerpt' => $item->description,
                        'post_status' => 'draft',
                        'post_type' => 'post',
                        'post_category' => array(1)
                    )
                );
                // We keep the video_id around to query against.
                update_post_meta($post_id, 'video_id', $video_id);
                update_post_meta($post_id, 'preview_image_url', $item->preview_image_url);
                // Now upload the thumbnail image to the system and associate
                $autopost->fetch_media($item->preview_image_url, $post_id);
                $cachebuster = '201504';
                $main_video = "
                    <div id='playerContainer'></div>
                    <script src='" . get_template_directory_uri() . "/scripts/ooyala.js?" . $cachebuster . "'></script>
                    <script>
                        OO.ready(function() { window.player = OO.Player.create('playerContainer', '" . $video_id . "', config);
                        load_video('" . $video_id . "'); });
                    </script>";
                update_post_meta($post_id, 'main_video', $main_video);

                echo "<p><strong>New post, " . $item->name . ", created</strong></br><a href='/wp-admin/post.php?post=" . $post_id . "&action=edit'>Edit it here</a>.</p>";
                echo "<p>This post is in draft mode, and has the default category assigned.</p>";
            else:
                echo "<h3>Looks like this video has already been imported.</h3>";
                $query->the_post();
                $response = "<p>You can edit the post, <a href='/wp-admin/post.php?post=" . get_the_ID() . "&action=edit'>" . get_the_title() . "</a>.</p>";
                echo $response;

            endif;
        endif;
    }
}
new debug_options_page;

class options_page
{
    function __construct()
    {
        $this->slug = 'ooyala-autopost';
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    function admin_menu()
    {
        add_options_page('Page Title', 'Ooyala Autopost Admin', 'manage_options', $this->slug, array($this, 'settings_page') );
    }

    function settings_page()
    {
        echo '
        <h1>Ooyala Autopost Admin</h1>
        <p>Functions here for handling the ooyala api-to-wordpress post plugin.</p>
        <ul>
            <li><a href="?page=' . $this->slug . '&amp;action=manual">Run autopost on all labels</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=sports-show">Run autopost on Sports Show</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=sports-show-mornings">Run autopost on Sports Show A.M.</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=nhl-colorado-avalanche">Run autopost on NHL</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=mlb-colorado-rockies">Run autopost on MLB</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=nba-denver-nuggets">Run autopost on NBA</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=nfl">Run autopost on NFL</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=colorado-rapids">Run autopost on Rapids / MLS</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=you-gotta-see-this">Run autopost on You Gotta See This</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=social-animals">Run autopost on Social Animals</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=pop-a-shot-sports-show">Run autopost on Pop-A-Shot</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=pick-six">Run autopost on Pick 6</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=more-with-les">Run autopost on Les</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=dope-of-the-day">Run autopost on Dope</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=the-big-stuff">Run autopost on Big Stuff</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=woodys-world">Run autopost on Woody\'s World</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=broncos">Run autopost on Broncos</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=guests">Run autopost on Guests</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=news">Run autopost on news</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=up-to-date">on Up To Date</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=sports">on Sports</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=denver-post-films">on Films</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=cannabist">on Cannabist</a></li>
        </ul>';

        if ( 'manual' == $_GET['action'] ):
            if ( !isset($_GET['label']) ):
                ooyala_execute_autopost();
            else:
                ooyala_execute_autopost(false, $_GET['label']);
            endif;
        endif;
    }
}
new options_page;


// *******************
//
// GET PARAMS WE USE TO CONTROL VIDEO FUNCTIONALITY AND DISPLAY
//
// *******************
add_action('init','iframe_register_param');
function iframe_register_param() 
{ 
    global $wp; 
    $wp->add_query_var('iframe'); 
    $wp->add_query_var('autoplay'); 
}
