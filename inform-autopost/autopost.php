<?php
/**
 * Plugin Name: Inform Autopost
 * Plugin URI: 
 * Description: Every time an Inform video from a particular playlist comes in, create a blog post for it.
 * Version: 0.1
 * Author: Joe Murphy
 * Author URI: 
 * License: Apache-2
 */

require(plugin_dir_path( __FILE__ ) . 'class.inform.php');
class Article
{
    // Methods for handling Denver Post article objects.
    var $url;
    var $title;

    function __construct()
    {
    }

    public function set_url($url)
    {
        // Set the value of the test var.
        return $this->url = $url;
    }

    public function lookup_title()
    {
        // Pull an article, take its pieces. 
        // Sometimes we just want the title, sometimes we want more.

        $content = $this->get_content($this->url);
        preg_match('/<h1 id="articleTitle" class="articleTitle">([^<]+)<\/h1>/', $content, $matches);
        $article['title'] = $matches[1];
        return $matches[1];
    }

    private function get_content($url)
    {
        $url=str_replace('&amp;','&',$url);
        $ch=curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $markup = curl_exec($ch);
        curl_close($ch);
        return $markup;
    }
}


class AutoPost
{

    static public function slugify($text)
    { 
        // From http://stackoverflow.com/questions/2955251/php-function-to-make-slug-url-string
        // replace non letter or digits by -
        $text = preg_replace('~[^\\pL\d]+~u', '-', $text);

        // trim
        $text = trim($text, '-');

        // transliterate
        $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

        // lowercase
        $text = strtolower($text);

        // remove unwanted characters
        $text = preg_replace('~[^-\w]+~', '', $text);

        if (empty($text))
        {
        return 'n-a';
        }

        return $text;
    }


    static public function fetch_media($file_url, $post_id) 
    {
        /* Import media from url
         *
         * @param string $file_url URL of the existing file from the original site
         * @param int $post_id The post ID of the post to which the imported media is to be attached
         *
         * @return boolean True on success, false on failure
         */
        // From http://nlb-creations.com/2012/09/26/how-to-programmatically-import-media-files-to-wordpress/
        require_once(ABSPATH . 'wp-load.php');
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        global $wpdb;

        if ( !$post_id ) return false;

        //directory to import to    
        $artDir = 'wp-content/uploads/importedmedia/';
        //$artDir = 'wp-content/uploads/';

        //if the directory doesn't exist, create it 
        //if ( !file_exists(ABSPATH.$artDir) ) mkdir(ABSPATH.$artDir);

        // rename the file... alternatively, you could explode on "/" and keep the original file name
        // Note: This assumer a file extension on the image.
        //$ext = array_pop(explode(".", $file_url));
        //if your post has multiple files, you may need to add a random number to the file name to prevent overwrites
        //$new_filename = 'photo-'.$post_id.".".$ext; 
        $new_filename = 'photo-'.$post_id.'.jpg';

        if (@fclose(@fopen($file_url, "r"))):
            // make sure the file actually exists
            $ch = curl_init($file_url);
            $fp = fopen(ABSPATH.$artDir.$new_filename, 'wb');
            curl_setopt($ch, CURLOPT_FILE, $fp);
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_exec($ch);
            curl_close($ch);
            fclose($fp);
            //file_put_contents(ABSPATH.$artDir.$new_filename, file_get_contents($file_url));

            $siteurl = get_option('siteurl');
            $file_info = getimagesize(ABSPATH.$artDir.$new_filename);

            //create an array of attachment data to insert into wp_posts table
            $artdata = array(
                'post_author' => 1, 
                'post_date' => current_time('mysql'),
                'post_date_gmt' => current_time('mysql'),
                'post_title' => $new_filename, 
                'post_status' => 'inherit',
                'comment_status' => 'closed',
                'ping_status' => 'closed',
                'post_name' => sanitize_title_with_dashes(str_replace("_", "-", $new_filename)),
                'post_modified' => current_time('mysql'),
                'post_modified_gmt' => current_time('mysql'),
                'post_parent' => $post_id,
                'post_type' => 'attachment',
                'guid' => $siteurl.'/'.$artDir.$new_filename,
                'post_mime_type' => $file_info['mime'],
                'post_excerpt' => '',
                'post_content' => ''
            );

            $uploads = wp_upload_dir();
            $save_path = $uploads['basedir'].'/importedmedia/'.$new_filename;
            //$save_path = $uploads['basedir'].$new_filename;

            //insert the database record
            $attach_id = wp_insert_attachment( $artdata, $save_path, $post_id );

            //generate metadata and thumbnails
            if ($attach_data = wp_generate_attachment_metadata( $attach_id, $save_path)) {
                wp_update_attachment_metadata($attach_id, $attach_data);
            }

            //optional make it the featured image of the post it's attached to
            $rows_affected = $wpdb->insert($wpdb->prefix.'postmeta', array('post_id' => $post_id, 'meta_key' => '_thumbnail_id', 'meta_value' => $attach_id));
        else: return false;
        endif;

        return true;
    }

}

function inform_execute($test=false)
{
    add_action('init', 'inform_execute_autopost');
}

function inform_execute_autopost($test=false, $playlist='', $file='playlists.csv')
{
    // Max posts per category ingested
    $limit = 2;

    // This gets run on the crontab.
    $playlists_raw = file(plugin_dir_path( __FILE__ ) . $file, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
    $playlists = array();
    foreach ( $playlists_raw as $item ):
        $items = explode(',', $item);
        // $item[0] is the slug of the playlist. 
        // $item[1] is the display name of the playlist.
        // $item[2] is the URL of the playlist RSS feed.
        // $item[3] is the playlist id.
        if ( trim($items[2]) !== '' ) $playlists[$items[0]] = array('url' => $items[2], 'id' => $items[3]);
    endforeach;
    $already_executing = get_transient('already_executing');

    // If we're passing a playlist, we only run this function on the playlist passed.
    if ( $playlist != '' ):
        $already_executing = FALSE;
        $limit = 15;
        $new_playlist[$playlist] = $playlists[$playlist];
        unset($playlists);
        $playlists = $new_playlist;
    endif;

    if ( $already_executing !== FALSE ):
        echo "<h2>Note: Already Executing. Wait up to 60 seconds before trying again.</h2>";
    endif;

    if ( $already_executing === FALSE ):
        // Make sure we don't run this on the crontab more than once at the same time.
        set_transient('already_executing', 1, 119);
        $api = new Inform();
        foreach ( $playlists as $slug => $list ):
            $category = get_category_by_slug($slug);
            if ( $category === false ):
                echo "<p><strong>Could not find matching category $slug, skipping this channel.</strong></p>";
                continue;
            endif;

            $api->slug = $slug;
            $feed = $api->get_xml($list['url']);
            $x = $limit;
            $i = 0;

            while ( $x >= 0 ):
                $x --;
                $item = $feed->get_item($i);

                // See if a post already exists with the video id.
                $video_id = trim($item->data['child']['http://base.google.com/cns/1.0']['VideoId'][0]['data']);
                $args = array( 'numberposts' => 1, 'meta_key' => 'video_id', 'meta_value' => $video_id, 'post_status' => array('publish', 'draft', 'auto-draft', 'future'));
                $query = new WP_Query($args);
                if ( $query->have_posts() == FALSE ):
                    $autopost = new AutoPost();
                    $post_id = -1;
                    $author_id = 1;

                    $slug = $autopost->slugify($item->get_title());
                    $title = $item->get_title();

                    $args = array( 'numberposts' => 1, 'meta_key' => 'video_id', 'meta_value' => $video_id, 'post_status' => array('publish', 'draft', 'auto-draft', 'future'));
                    $query = new WP_Query($args);
                    if ( count($query->posts) == 0 ):
                        $post_id = wp_insert_post(
                            array(
                                'post_author' => $author_id,
                                'post_name' => $slug,
                                'post_title' => $title,
                                'post_content' => $item->get_description(),
                                'post_excerpt' => $item->get_description(),
                                'post_status' => 'draft',
                                'post_type' => 'post',
                                'post_category' => array($category->term_id)
                            )
                        );
                        // We keep the video_id around to query against.
                        update_post_meta($post_id, 'video_id', $video_id);
                        update_post_meta($post_id, 'preview_image_url', 'http://thumbnail.newsinc.com/' . $video_id . '.sf.jpg');

                        // Now upload the thumbnail image to the system and associate
                        $autopost->fetch_media('http://thumbnail.newsinc.com/' . $video_id . '.sf.jpg', $post_id);
                        $cachebuster = '201510';
                        $main_video = "
                            <div id='mainplayer' class='ndn_embed' data-config-widget-id='1' data-config-type='VideoPlayer/Single' data-config-tracking-group='90115' data-config-playlist-id='" . $list['id'] . "' data-config-video-id='$video_id' data-config-site-section='section' data-config-height='9/16w'></div>
                            <script src='" . get_template_directory_uri() . "/scripts/inform.js?" . $cachebuster . "'></script>
                            ";
                        update_post_meta($post_id, 'main_video', $main_video);
                    endif;
                endif;
                $i ++;
            endwhile;
        endforeach;
    endif;
}

// *******************
//
// CRONTAB
//
// *******************

// We do this to get the Inform video-posts published every 60 minutes
// On an early action hook, check if the hook is scheduled - if not, schedule it.
add_filter( 'cron_schedules', 'inform_add_custom_cron_schedule' );
function inform_add_custom_cron_schedule( $schedules ) 
{
    $schedules['twominute'] = array(
        'interval' => 120,
        'display'  => __( 'Every 2 Minutes' ),
    );
    $schedules['fiveminute'] = array(
        'interval' => 300,
        'display'  => __( 'Every 5 Minutes' ),
    );
    $schedules['30min'] = array(
        'interval' => 1800,
        'display'  => __( 'Every 30 Minutes' ),
    );
    return $schedules;
}

add_action( 'wp', 'inform_crontab_setup' );
function inform_crontab_setup() 
{
    if ( !wp_next_scheduled( 'autopost_event' ) ):
        $return = wp_schedule_event(time(), 'twominute', 'autopost_event');
    endif;
}
add_action( 'autopost_event', 'inform_execute_autopost' );
//wp_clear_scheduled_hook('autopost_event');


// *******************
//
// WP-ADMIN OPTIONS PAGE
//
// *******************
class debug_options_page
{
    function __construct()
    {
        $this->slug = 'inform-debug';
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    function admin_menu() { add_options_page('Page Title', 'Inform: Debug', 'manage_options', $this->slug, array($this, 'settings_page') ); }

    function settings_page()
    {
        echo '
        <h1>Inform Debug</h1>
        <p>Figure out where a video you need published is, and publish it if need be.</p>
        <form method="GET" action="./options-general.php">
            <p><label for="video_id">Inform Video ID:</label><input type="text" id="video_id" name="video_id" maxlength="50" value="" /></p>
            <input type="hidden" name="page" value="inform-debug" />
            <input type="submit" value="Look Up Video ID">
        </form>
        ';
        if ( isset($_GET['video_id']) ):
            $video_id = trim(htmlspecialchars($_GET['video_id']));
            $args = array( 'numberposts' => 1, 'meta_key' => 'video_id', 'meta_value' => $video_id, 'post_status' => array('publish', 'draft', 'auto-draft', 'future'));
            $query = new WP_Query($args);
            if ( $query->have_posts() == FALSE ):
                echo '<p style="color:red">Video ' . $video_id . ' was not found in any existing posts. Attempting to import it now.</p>';
                // Get the inform video information from inform
                $api = new Inform();
                $item = $api->api->get('/v2/assets/' . $video_id);
                var_dump($item);

                $autopost = new AutoPost();
                $post_id = -1;
                $author_id = 1;
                $slug = $autopost->slugify($item->name);
                $post_id = wp_insert_post(
                    array(
                        'post_author' => $author_id,
                        'post_name' => $item->name,
                        'post_title' => $item->name,
                        'post_content' => $item->description,
                        'post_excerpt' => $item->description,
                        'post_status' => 'draft',
                        'post_type' => 'post',
                        'post_category' => array(1)
                    )
                );
                // We keep the video_id around to query against.
                update_post_meta($post_id, 'video_id', $video_id);
                update_post_meta($post_id, 'preview_image_url', $item->preview_image_url);
                // Now upload the thumbnail image to the system and associate
                $autopost->fetch_media($item->preview_image_url, $post_id);
                $cachebuster = '201507';
                $main_video = "
                    <div id='playerContainer'></div>
                    <script src='" . get_template_directory_uri() . "/scripts/inform.js?" . $cachebuster . "'></script>
                    <script>
                        OO.ready(function() { window.player = OO.Player.create('playerContainer', '" . $video_id . "', config);
                        load_video('" . $video_id . "'); });
                    </script>";
                update_post_meta($post_id, 'main_video', $main_video);

                echo "<p><strong>New post, " . $item->name . ", created</strong></br><a href='/wp-admin/post.php?post=" . $post_id . "&action=edit'>Edit it here</a>.</p>";
                echo "<p>This post is in draft mode, and has the default category assigned.</p>";
            else:
                echo "<h3>Looks like this video has already been imported.</h3>";
                $query->the_post();
                $response = "<p>You can edit the post, <a href='/wp-admin/post.php?post=" . get_the_ID() . "&action=edit'>" . get_the_title() . "</a>.</p>";
                echo $response;
                   
            endif;
        endif;
    }
}
new debug_options_page;

class label_options_page
{
    function __construct()
    {
        $this->slug = 'inform-label';
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    function admin_menu() { add_options_page('Page Title', 'Inform: Labels', 'manage_options', $this->slug, array($this, 'settings_page') ); }

    function settings_page()
    {
        echo '
        <h1>Inform Labels List</h1>
        <p>A list of labels and the label\'s corresponding inform id.</p>
        <ul class="labels">
        ';
        $api = new Inform();
        $items = $api->get_playlists();
        $playlists = array();
        foreach ( $items->items as $key => $value ):
            $playlists[$value->id] = $value->full_name;
        endforeach;

        asort($playlists);
        foreach ( $playlists as $key => $value ):
            echo '<li>';
            echo '<span>' . $value . '</span>: ' . $key;
            echo '</li>';
        endforeach;
        echo '</ul>';
    }
}
new label_options_page;

class autopost_options_page
{
    function __construct()
    {
        $this->slug = 'inform-autopost';
        add_action('admin_menu', array($this, 'admin_menu'));
    }

    function admin_menu() { add_options_page('Page Title', 'Inform Autopost', 'manage_options', $this->slug, array($this, 'settings_page') ); }

    function settings_page()
    {
        echo '
        <h1>Inform Autopost Admin</h1>
        <p>Functions here for handling the inform api-to-wordpress post plugin.</p>
        <ul>
            <li><a href="?page=' . $this->slug . '&amp;action=manual">Run autopost on all labels</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=sports-show">Run autopost on Sports Show</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=sports-show-mornings">Run autopost on Sports Show A.M.</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=nhl-colorado-avalanche">Run autopost on NHL</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=mlb-colorado-rockies">Run autopost on MLB</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=nba-denver-nuggets">Run autopost on NBA</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=nfl">Run autopost on NFL</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=colorado-rapids">Run autopost on Rapids / MLS</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=you-gotta-see-this">Run autopost on You Gotta See This</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=social-animals">Run autopost on Social Animals</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=pop-a-shot-sports-show">Run autopost on Pop-A-Shot</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=pick-six">Run autopost on Pick 6</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=more-with-les">Run autopost on Les</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=dope-of-the-day">Run autopost on Dope</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=the-big-stuff">Run autopost on Big Stuff</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=woodys-world">Run autopost on Woody\'s World</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=broncos">Run autopost on Broncos</a></li>
             <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=guests">Run autopost on Guests</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=news">Run autopost on news</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=up-to-date">on Up To Date</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=sports">on Sports</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=denver-post-films">on Films</a></li>
            <li><a href="?page=' . $this->slug . '&amp;action=manual&amp;label=cannabist">on Cannabist</a></li>
        </ul>';

        if ( 'manual' == $_GET['action'] ):
            if ( !isset($_GET['label']) ):
                inform_execute_autopost();
            else:
                inform_execute_autopost(false, $_GET['label']);
            endif;
        endif;
    }
}
new autopost_options_page;



// *******************
//
// GET PARAMS WE USE TO CONTROL VIDEO FUNCTIONALITY AND DISPLAY
//
// *******************
add_action('init','iframe_register_param');
function iframe_register_param() 
{ 
    global $wp; 
    $wp->add_query_var('iframe'); 
    $wp->add_query_var('https'); 
    $wp->add_query_var('autoplay'); 
    $wp->add_query_var('autonext'); 
    $wp->add_query_var('chromeless'); 
}
